// Fichier à revoir....Plein de fautes
// Tableau des captures de valeurs :
int samples[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

// Pour convertir valeur en Température
float conversion( int V) {
  float T = map (V, 0, 1023, 0, 50);
  return T;
}

// décalage des valeurs vers l'arrière du tableau.
void shift_samples() {
  for (int i = 11; i > 0; i--) {
    samples[i ] = samples[i - 1];
  }
}


void setup() {
  Serial.begin(9600);
  Serial.println("*******************");

  int x_base = 500;
  float x_converti = conversion(x_base);
  // Entrée analogque (samples):
  pinMode(A0, INPUT);

  // Pin d'envoi d'alarme :
  pinMode(9, OUTPUT);
}

void control() {
  int tt = 0;
  int nb_al=0;
  // FAire la somme des échantillons :
  for (int n = 0; n <= 11; n++) {
    if (conversion(samples[n]) >= 30 ) {
     nb_al++;
    } else{
      nb_al=0;
    }
  }
  // Si  la somme est > 360, on est en alerte :
  if (nb_al >= 11) {
    send_alarm();
  }
}

void send_alarm() {
  for (int i = 0; i  <= 2 ; i++) {
    digitalWrite(9, HIGH);
    delay(250);
    digitalWrite(9, LOW);
    delay(100);
  }
  Serial.println("***** ALARME *****");
}
void print_samples() {
  for (int i = 0; i <= 11; i++) {
    Serial.print(conversion(samples[i]));
    Serial.print(",");
  }
  Serial.println(' ');

}
void loop() {
  // 1- décalage des variables :
  shift_samples();
  // 2- On échantillone une nouvelle valeur :
  samples[0] = analogRead(A0);
  print_samples();
  // 3 - on controle les échantilons :
  control();
  // On attend 5mn :
  //delay(5 * 60 * 1000);
  delay(1);
}
